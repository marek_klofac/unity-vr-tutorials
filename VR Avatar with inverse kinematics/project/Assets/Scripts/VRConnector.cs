﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VRConnector
{
    public Transform vrTarget;
    public Transform modelTarget;
    public Vector3 positionOffset;
    public Vector3 rotationOffset;

    public void Synchronize()
    {
        modelTarget.position = vrTarget.TransformPoint(positionOffset);
        modelTarget.rotation = vrTarget.rotation * Quaternion.Euler(rotationOffset);
    }
}
