﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRRig : MonoBehaviour
{
    public Transform headConstraint;
    public VRConnector head;
    public VRConnector leftHand;
    public VRConnector rightHand;
    private Vector3 headBodyOffset;


    // Start is called before the first frame update
    void Start()
    {
        headBodyOffset = transform.position - headConstraint.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = headConstraint.position + headBodyOffset;
        transform.forward = Vector3.ProjectOnPlane(headConstraint.up, Vector3.up).normalized;
        head.Synchronize();
        leftHand.Synchronize();
        rightHand.Synchronize();
    }
}

