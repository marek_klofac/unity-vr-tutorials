﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class Draw : MonoBehaviour
{
    public Transform drawPositionSource;
    public float lineWidth = 0.03f;
    public Material lineMaterial;
    public float distanceThreshold = 0.05f;

    private bool isDrawing;
    private LineRenderer currentLine;
    private List<Vector3> currentLinePositions = new List<Vector3>();

    // Update is called once per frame
    void Update()
    {
        if(isDrawing)
        {
            UpdateDrawing();
        }
    }
  
    public void StartDrawing()
    {
        isDrawing = true;

        // Create new line
        GameObject line = new GameObject("Line");
        currentLine = line.AddComponent<LineRenderer>();

        UpdateLine();
    }
    public void StopDrawing()
    {
        isDrawing = false;
        currentLinePositions.Clear();
        currentLine = null;
    }

    private  void UpdateDrawing()
    {
        if(!currentLine || currentLinePositions.Count == 0)
        {
            return;
        }
        Vector3 lastSetPosition = currentLinePositions[currentLinePositions.Count - 1];
        if(Vector3.Distance(lastSetPosition, drawPositionSource.position) > distanceThreshold)
        {
            UpdateLine();
        }
    }

    private void UpdateLine()
    {
        // Add created starting line to list of lines
        currentLinePositions.Add(drawPositionSource.position);
        currentLine.positionCount = currentLinePositions.Count;
        currentLine.SetPositions(currentLinePositions.ToArray());

        // Update the visual of line
        currentLine.startWidth = lineWidth;
        currentLine.material = lineMaterial;
    }
}
